id=10;
od=id+18;
thickness=10;
$fn=90;
nut_d=6.4;

intersection()
{
    translate([0, 0, thickness/2])
    intersection()
    {
        difference()
        {
            // main body shape
            cylinder(d=od, h=thickness, center=true);
            cylinder(d=id, h=thickness, center=true);

            // M3 screw holes
            translate([od/2-5,0,0])
            rotate([90,0,0])
                cylinder(d=3.4, h=od, center=true);
            translate([-od/2+5,0,0])
            rotate([90,0,0])
                cylinder(d=3.4, h=od, center=true);

            // M3 nut inserts
            translate([od/2-5,-od/4-2,0])
            rotate([90,0,0])
                cylinder(d=nut_d, h=od/2, $fn=6, center=true);
            translate([-od/2+5,od/4+2,0])
            rotate([90,0,0])
                cylinder(d=nut_d, h=od/2, $fn=6, center=true);

            // M3 screw inserts
            translate([od/2-5,od/4+2,0])
            rotate([90,0,0])
                cylinder(d=nut_d, h=od/2, center=true);
            translate([-od/2+5,-od/4-2,0])
            rotate([90,0,0])
                cylinder(d=nut_d, h=od/2, center=true);
        }

        translate([0, od/4, 0])
            cube([od, od/2, thickness], center=true);
    }

    // clear rear-mounted blower on carriage
    translate([-od/2,0,0])
        cube([od, od/2-4, thickness]);
}