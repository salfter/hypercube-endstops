Hypercube Shaft-Mount Endstops
==============================

These are simple rings that clamp onto the shafts of the CoreXY mechanism to
set travel limits when using the StallGuard feature of TMC2130 drivers
instead of limit switches or other endstops.  Print two of each to make a
set suitable for a Hypercube 300 (10-mm carbon-fiber tubes on X, 8-mm steel
rods on Y).  Use M3x8 screws and M3 nuts to secure the two halves to each
shaft.  Modify and re-render the OpenSCAD files if you need different
diameters.
